<?php


namespace Data\Repositories;
use Business\Models\UserModel;


/**
 * Class UsersRepository
 * @package Data\Repositories
 * @method static UserModel[] Get
 * @method static UserModel GetOne
 */
class UsersRepository extends BaseRepository {

}