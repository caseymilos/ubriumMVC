<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 27.4.2015.
 * Time: 11:47
 */

namespace Data\Repositories;

use Business\Models\RolePermissionModel;
use Data\Database\MysqliDb;

/**
 * Class RolePermissionsRepository
 * @package Data\Repositories
 * @method static RolePermissionModel[] Get
 * @method static RolePermissionModel GetOne
 */
class RolePermissionsRepository extends BaseRepository {

    const COLUMN_ROLE_PERMISSION_ID = 'RolePermissionId';
    const COLUMN_ROLE_ID = 'RoleId';
    const COLUMN_PERMISSION_ID = 'PermissionId';

    /**
     * Returns set of permissions
     *
     * @param [] $wheres
     * @return int[]
     */
    public static function GetPermissions($roleId) {
        $result = [];

        $db = MysqliDb::getInstance();

        $db->where("RoleId", $roleId);
        $permissions = $db->get('role_permissions');

        if (is_array($permissions) && count($permissions) > 0) {
            foreach ($permissions as $permissionData) {
                $result[] = (int)$permissionData['PermissionId'];
            }
        }
        return $result;

    }


}