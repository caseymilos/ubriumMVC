<?php
spl_autoload_extensions(".php");
spl_autoload_register('autoloader');

define("BASE_PATH", dirname(__FILE__));

function autoloader($className)
{
    $path = str_replace("\\", "/", BASE_PATH . "/" . $className . '.php');
    if (is_readable($path)) {
        include $path;
    }
}