<?php
try {


	include('../../../GlobalConfig.php');
	include('Components/Autoload.php');
	include('../../../GlobalAutoload.php');
	Session::Start();

	$router = new Router();
	if (Config::maintenanceMode && "Maintenance" != $router->Controller) {
		Router::Redirect("Maintenance");
	}

	$router->RenderPage();
} catch (MVCException $e) {
	$e->DisplayError();
} catch (Exception $e) {
	var_dump($e);
	die();
}
