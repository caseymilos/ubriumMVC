<?php


class HomeController extends MVCController {

	public function GetIndex() {

		$model = new LandingViewModel();

		$model->Projects = \Business\ApiControllers\ProjectsApiController::GetProjects();

		$this->RenderView("Landing/Landing", ["model" => $model]);

	}

	public function GetAbout() {

		$this->RenderView("About/About");

	}

	public function GetReferrals() {

		$this->RenderView("Referrals/Referrals");

	}public function GetItem() {

		$this->RenderView("Item/Item");

	}

	public function GetLanding() {

		$this->RenderView("Landing/Landing");

	}

	public function GetProfile() {

		$this->RenderView("Profile/Profile");

	}


}