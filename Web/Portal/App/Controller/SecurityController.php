<?php

use Business\DTO\CurrentUserDTO;
use Business\Security\Crypt;
use Data\Repositories\UsersRepository;

class SecurityController extends MVCController{

	public function GetLogIn() {
		$this->RenderView("User/LogIn");
	}

	public function PostLogIn($username, $password) {
		$user = UsersRepository::GetOne(["Username" => $username]);
		if (!is_null($user)) {
			if (Crypt::CheckPassword($password, $user->Password)) {
				// Packing CurrentUserDTO from user
				$userDto = new CurrentUserDTO();

				$userDto->UserId = $user->UserId;
				$userDto->Username = $user->Username;
				$userDto->FirstName = $user->FirstName;
				$userDto->LastName = $user->LastName;
				$userDto->Picture = $user->Picture;
				$userDto->RoleId = $user->RoleId;

				Security::SetCurrentUser($userDto);

				Router::Redirect("");
			}
		}
	}

	public function GetLogout() {
		Session::Remove("CurrentUser");
		Router::Redirect("login");
	}

}