<?php


class Html {

    public static function RenderPartial($partial, $vars = array(), $display = true) {
        extract($vars);
        ob_start();
        include("View/" . $partial . ".php");
        $content = ob_get_clean();
        if($display) {
            echo $content;
            return true;
        }
        else {
            return $content;
        }
    }

} 