<?php

/**
 * Class RoutesConfig
 */
class RoutesConfig {

	/**
	 * @return RouteDTO[]
	 */
	public static function GetRoutes() {
		$routes = array(
			"home" => new RouteDTO("home", "Home", "Index"),
			"about" => new RouteDTO("about", "Home", "About"),
			"referrals" => new RouteDTO("referrals", "Home", "Referrals"),
			"item" => new RouteDTO("item", "Home", "Item"),
			"landing" => new RouteDTO("landing", "Home", "Landing"),
			"profile" => new RouteDTO("profile", "Home", "Profile"),

			/** User */
			"login" => new RouteDTO("login", "Security", "LogIn"),
			"logout" => new RouteDTO("logout", "Security", "Logout"),
		);
		return $routes;
	}

}