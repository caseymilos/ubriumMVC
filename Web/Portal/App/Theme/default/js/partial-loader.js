$("[data-partial-load]").each(function () {
	var $this = $(this);
	$.ajax({
		url: $this.data('partialLoad'),
		success: function (content) {
			$this.html(content);
			if ($this.data("callback") != 'undefined') {
				var functionName = $this.data("callback");
				setTimeout(functionName + "()", 1);
			}
		}
	});

});