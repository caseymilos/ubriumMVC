<?php

class MainHelper {

	public static function IsLoggedIn() {
		return Security::GetCurrentUser() == false ? false : true;
	}

	public static function GetCurrentUser() {
		return Security::GetCurrentUser();
	}

	public static function Enum($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::Description($value);
	}

	public static function EnumCaption($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		$enum = new $type();
		return $enum->Caption($value);
	}

	public static function EnumConstant($enum, $constant) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::GetConstant($constant);
	}

	public static function ProductPicture($id) {
		switch ($id) {
			case 1:
				$picture = "coca-cola.png";
				break;
			case 2:
				$picture = "fanta.png";
				break;
			case 3:
				$picture = "sprite.png";
				break;
			default:
				$picture = "no-image.png";
				break;
		}

		return sprintf("%s/Media/Products/%s", Config::baseurl, $picture);
	}

}