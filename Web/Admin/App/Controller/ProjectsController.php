<?php
use Business\ApiControllers\ProjectsApiController;
use Business\Models\ProjectModel;

class ProjectsController extends MVCController{
	public function PostCreateProject($title, $description, $picture = null)
	{
		$currentUser = Security::GetCurrentUser();
		$model = new ProjectModel();
		$model->Name = $title;
		$model->Description = $description;
		$model->DateCreated = date("Y-m-d H:i:s");
		$model->UserId = $currentUser->UserId;
		if ($picture['name'] !== "") {
			$newName = self::_generatePictureName($picture['name'], $title);
			move_uploaded_file($picture['tmp_name'], self::_generatePictureFullPath($newName));
			$model->Image = $newName;
		}
		ProjectsApiController::InsertProject($model);
		Router::Redirect("dashboard");
	}
	private static function _generatePictureFullPath($pictureName)
	{
		return sprintf("%sMedia/projects/%s", CDN_PATH, $pictureName);
	}
	private static function _generatePictureName($logo, $name)
	{
		return CommonHelper::StringToFilename(sprintf("project-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($logo)));
	}
}