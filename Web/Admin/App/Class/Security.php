<?php

use Business\DTO\CurrentUserDTO;
use Business\Enums\RolesEnum;

class Security
{

    public static function CheckPermissions(array $permissions)
    {
        $userPermissions = $_SESSION['User']->Permissions;
        if (count(array_intersect($userPermissions, $permissions))) {
            return true;
        } else {
            return false;
        }
    }

    public static function RoleDefaultPageUrl($roleId = null)
    {
        $redirectUrl = "";

        switch ($roleId) {
            case RolesEnum::User:
                $redirectUrl = "Security/Login";
                break;
            case RolesEnum::Visitor:
                $redirectUrl = "Home";
                break;
            default:
                break;
        }

        return $redirectUrl;
    }



    /**
     * @return CurrentUserDTO|bool
     */
    public static function GetCurrentUser() {
        return Session::Get("CurrentUser");
    }

    /**
     * @param CurrentUserDTO $user
     */
    public static function SetCurrentUser($user) {
        Session::Set("CurrentUser", $user);
    }


} 