<?php

/**
 * Class RoutesConfig
 */
class RoutesConfig {

	/**
	 * @return RouteDTO[]
	 */
	public static function GetRoutes() {
		$routes = array(
			"home" => new RouteDTO("home", "Home", "Index"),

			/** User */
			"login"=> new RouteDTO("login", "Security", "Login"),
			"logout"=> new RouteDTO("logout", "Security", "Logout"),
			"register" => new RouteDTO("registration", "User", "Register"),
			"dashboard" => new RouteDTO("dashboard", "User", "Dashboard"),
			"create-project" => new RouteDTO("create-project", "Projects", "CreateProject"),
			"confirmation-link-sent" => new RouteDTO("confirmation-link", "User", "ConfirmationLinkSent"),
			"successful-registration" => new RouteDTO("successful-register", "User", "SuccessfulRegistration"),
			"unsuccessful-registration" => new RouteDTO("unsuccessful-register", "User", "UnsuccessfulRegistration"),
			"reset-user-password" => new RouteDTO("reset-password/{guid:string}", "Security", "ResetPassword"),



		);
		return $routes;
	}

}