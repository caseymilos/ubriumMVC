<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 05-Aug-15
 * Time: 12:34
 */

namespace Business\Helper;

use Business\ApiControllers\NotificationsApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Lib\PHPMailer;

class NotificationHelper {

    /**
     * @param string $subject Subject of email
     * @param string $body Body of email
     * @param string $text Description for admin area
     * @param array $attachments Attachments for mail
     * @param SenderDTO[] $senders List of senders
     * @param RecipientDTO[] $recipients List of recipients
     * @param RecipientDTO[] $cc List of CC recipients
     * @param RecipientDTO[] $bcc List of BCC recipients
     * @throws \Business\Lib\PHPMailerException
     * @throws \Exception
     */
    public static function Send($subject, $body, $text, $senders, $recipients, $attachments = [], $cc = [], $bcc = []) {
        // Saving to DB
        /**
         * $model = new MailViewModel();
         * $model->Sender = $senders;
         * $model->Text = $text;
         * $model->Subject = $subject;
         * $model->Recipients = $recipients;
         * $model->Attachments = $attachments;
         * $model->CC = $cc;
         * $model->BCC = $bcc;
         * $model->DateSent = date("Y-m-d H:i:s");
         * NotificationsApiController::InsertMessage($model);
         **/

        // Sending mail
        $mail = new PHPMailer();
		$mail->CharSet = "UTF-8";

        if (count($recipients) > 0) {
            foreach ($recipients as $recipient) {
                $mail->addAddress($recipient->Address, $recipient->Name);
            }
        }
        if (count($cc) > 0) {
            foreach ($cc as $recipient) {
                $mail->addCC($recipient->Address, $recipient->Name);
            }
        }
        if (count($bcc) > 0) {
            foreach ($bcc as $recipient) {
                $mail->addBCC($recipient->Address, $recipient->Name);
            }
        }

        $mail->Subject = $subject;

        if (count($senders) > 0) {
            foreach ($senders as $sender) {
                $mail->setFrom($sender->Address, $sender->Name);
            }
        }
        $mail->isHTML(true);
        $mail->Body = $body;


        // SMTP settings
        if (USE_SMTP == true) {
            $mail->isSMTP();
            $mail->Host = SMTP_HOST;
            $mail->SMTPAuth = true;
            $mail->Port = SMTP_PORT;
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
        }

        $mail->send();
    }
}