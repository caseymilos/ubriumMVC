<?php
namespace Business\Models;

class UserModel {

	public $UserId;
	public $Email;
	public $Password;
	public $FirstName;
	public $LastName;
	public $Picture;
	public $RegistrationDate;
	public $Username;
	public $ConfirmRegistration;
}