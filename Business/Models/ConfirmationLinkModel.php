<?php
namespace Business\Models;

use DateTime;
/**
 * Class ConfirmationLinkModel
 * @package Business\Models
 * @property integer $ConfirmationLinkId
 * @property integer $UserId
 * @property String $ExpirationDate
 * @property String $ConfirmationLink
 */
class ConfirmationLinkModel {

	public $ConfirmationLinkId;
	public $UserId;
	public $ExpirationDate;
	public $ConfirmationLink;

	function __construct()
	{
		$this->ExpirationDate = new DateTime();
		$this->ExpirationDate->modify("+168 hours"); //seven days
		$this->ExpirationDate = $this->ExpirationDate->format("Y-m-d H:i:s");
	}
}