<?php

namespace Business\Models;

class ProjectPictureModel {

	public $ProjectPictureId;
	public $ProjectId;
	public $Picture;
	public $Order;
	public $UploadDate;

	public function PictureUrl() {
		return sprintf("%s/Media/Projects/ProjectsPictures/%s", CDN_URL, $this->Picture);
	}

}