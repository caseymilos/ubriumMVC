<?php

namespace Business\Enums;


use ReflectionClass;

abstract class BaseEnum
{

    public $descriptions = array();

    public static function enum() 
    {
        $reflect = new ReflectionClass( get_called_class() );
        return $reflect->getConstants();
    }

	public static function Caption($id) {
		$class = get_called_class();
		$enum = new ReflectionClass($class);
		$enum = $enum->newInstanceWithoutConstructor();
		foreach ($enum->enum() as $key => $value) {
			if ($value == $id) return $key;
		}
		return false;
	}

    public static function GetConstant($constant) {
        $class = get_called_class();
        $constant = str_replace(" ", "", $constant);
        $reflectionClass = new ReflectionClass($class);
        if($reflectionClass->hasConstant($constant)) {
            return $reflectionClass->getConstant($constant);
        }
        else {
            return null;
        }
    }

    public static function Description($id)
    {
        $class = get_called_class();
        $reflectionClass = new ReflectionClass($class);
        $enum = $reflectionClass->newInstance();
        if(isset($enum->Descriptions[$id])) {
            return $enum->Descriptions[$id];
        }
        return $enum->Caption($id);
    }



}
?>