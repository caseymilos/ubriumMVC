<?php

namespace Business\Security;

use Business\Models\UserModel;
use Data\DataManagers\UsersDataManager;
use Data\Repositories\UserAccessTokensRepository;
use Data\Repositories\UsersRepository;

/**
 * Class Users
 * @package Security
 *
 * Handles user management tasks
 */
class Users {

    /**
     * Login based on username and password. Returns complete user data.
     *
     * @param string $email
     * @param string $password
     *
     * @return UserModel|null
     */
    public static function LoginAdmin($email, $password) {
        $user = UsersRepository::GetOne([UsersRepository::COLUMN_USERNAME => $email]);
        if (!is_null($user)) {
            if (Crypt::CheckPassword($password, $user->Password)) {
                // Successful login

                // Populating permissions
                $userRoles = UsersDataManager::GetRoles($user->UserId);

                $user->Permissions = UsersDataManager::GetRolePermissions(current($userRoles)->RoleId);

                // Saving new UserAccessToken to database
                UsersDataManager::CreateUserAccessToken($user->UserId);

                return $user;
            }
        }

        return null;
    }

    /**
     * Login based on username and password. Returns complete user data.
     *
     * @param string $email
     * @param string $password
     *
     * @return UserModel|null
     */
    public static function Login($username, $password, $logInFromAdmin = false) {
        $user = UsersRepository::GetOne(["Username" => $username]);
        if (!is_null($user)) {
            if (Crypt::CheckPassword($password, $user->Password) || $logInFromAdmin) {
                // Successful login

                // Populating permissions
                $userRoles = UsersDataManager::GetRoles($user->UserId);

                $user->Permissions = UsersDataManager::GetRolePermissions(current($userRoles)->RoleId);

                // Saving new UserAccessToken to database
                UsersDataManager::CreateUserAccessToken($user->UserId);

                return $user;
            }
        }

        return null;
    }

    /**
     * Logs in user with access token. Returns complete user data.
     *
     * @param $token
     * @return UserModel|null
     */
    public static function LoginWithToken($token) {
        $token = UserAccessTokensRepository::GetOne([UserAccessTokensRepository::COLUMN_TOKEN => $token]);
        if ($token->IsActive()) {
            $user = UsersRepository::GetOne(['UserId' => $token->UserId]);
            $userRoles = UsersDataManager::GetRoles($user->UserId);

            $user->Permissions = UsersDataManager::GetRolePermissions(current($userRoles)->RoleId);
            return $user;
        }
        return null;

    }


    /**
     * Creates new User, assigns new created UserId and returns User object. Returns false if unsuccessful.
     *
     * @param UserModel $user
     *
     * @return bool|UserModel
     */
    public static function Register(UserModel $user) {
        $userId = UsersRepository::Insert([
            UsersRepository::COLUMN_USERNAME => $user->Username,
            UsersRepository::COLUMN_PASSWORD => Crypt::HashPassword($user->Password),
            UsersRepository::COLUMN_FIRST_NAME => $user->FirstName,
            UsersRepository::COLUMN_LAST_NAME => $user->LastName,
            UsersRepository::COLUMN_PICTURE => $user->Picture,
            UsersRepository::COLUMN_ROLE_ID => $user->RoleId,
            UsersRepository::COLUMN_USER_STATUS_ID => $user->UserStatusId,
            UsersRepository::COLUMN_EMAIL => $user->Email
        ]);

        if ($userId === false) {
            return false;
        }

        $user->UserId = $userId;
        return $user;
    }


}