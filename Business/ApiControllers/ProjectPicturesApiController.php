<?php

namespace Business\ApiControllers;

use Data\DataManagers\ProjectPicturesDataManager;

class ProjectPicturesApiController {

	public static function GetProjectPictures($projectId) {
		return ProjectPicturesDataManager::GetProjectPictures($projectId);
	}

	public static function GetOneProjectPictures($projectPictureId) {
		return ProjectPicturesDataManager::GetOneProjectPictures($projectPictureId);
	}

	public static function InsertProjectPicture($projectPicture){
		return ProjectPicturesDataManager::InsertProjectPicture($projectPicture);
	}

	public static function UpdateProjectPicture($projectPicture){
		return ProjectPicturesDataManager::UpdateProjectPicture($projectPicture);
	}

	public static function DeleteProjectPicture($projectPictureId){
		return ProjectPicturesDataManager::DeleteProjectPicture($projectPictureId);
	}

	public static function UpdatePicturesOrder($orders) {
		return ProjectPicturesDataManager::UpdatePictureOrder($orders);
	}

}